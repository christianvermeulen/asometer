<!doctype html>
<html>
<head>
	<title>Asometer | Hoe asociaal ben jij?</title>
	<meta name="description" content="Hoe asociaal wordt jij door social media? Wij berkenen het voor je!">
	<meta name="author" content="Christianvermeulen.net">
	<link href="/css/style.css" rel="stylesheet"/>
	<link href="/css/apprise.min.css" rel="stylesheet"/>
	<link rel="shortcut icon" href="/img/favicon.gif">
	<script src="/js/jquery-1.3.2.min.js"></script>
	<script src="/js/apprise-1.5.min.js"></script>
</head>
<body>
<div class="asometer">
	<img alt="Asometer" src="/img/logo.png"/>
	<img alt="Hoe asociaal ben jij?" src="/img/sublogo.png"/>
	
	<p>Je hebt dus geen Foursquare?? SHAME ON YOU! (maar je bent in ieder geval niet asociaal!)<br/> Je kunt deze score gebruiken voor het spel:</p>
	<p><span class='factor'>Jij bent <?php echo rand(0,100);?>% Aso!</p>
	
	<div class="social">
	
	<a href="http://twitter.com/share" class="twitter-share-button" data-url="http://<?php echo $_SERVER['HTTP_HOST'];?>" data-text="Hoe asociaal ben jij geworden door Social Media?" data-count="vertical" data-via="Asometer" data-related="Christian__V:Developer van Asometer">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	
	<iframe src="http://www.facebook.com/plugins/like.php?app_id=193687547346815&amp;href=http%3A%2F%2Fasometer.nl&amp;send=false&amp;layout=box_count&amp;width=50&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=60" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:60px;margin-right:20px;" allowTransparency="true"></iframe>
	
	<!-- Place this tag in your head or just before your close body tag -->
	<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
	  {lang: 'nl'}
	</script>
	
	<!-- Place this tag where you want the +1 button to render -->
	<g:plusone size="tall" href="http://asometer.nl"></g:plusone>
	
	</div>
</div>
<div class="footer">
	<p><a href="#" onclick="apprise('<?php
		echo "<h1>Wat is Asometer?</h1>";
		echo "<p>Asometer kijkt naar je afgelopen 100 checkins op foursquare en waar deze zijn. Door te kijken hoeveel van je checkins \&quot;asociaal\&quot; zijn berekent hij een asofactor.</p>";
		echo "<h1>Waarom is er een Asometer?</h1>";
		echo "<p>De smartphone markt explodeert, mobiel dataverkeerd breekt alle records en iedereen is in de ban van social media. Maar de maatschappij weet nog niet goed wat ze ermee aan moet.";
		echo " Wat vind men nou wel asociaal en wat niet? Om de maatschappij een handje te helpen om hier normen en waarden voor op te stellen hebben wij de asometer in het leven geroepen.";
		echo " Hetgeen de asometer als asociaal ziet hoeft dus niet <strong>per definitie</strong> asociaal te zijn. Het moet je alleen aan het denken zetten om zelf tot een conclusie te komen!</p>";
		echo "<h1>Wie zit er achter de Asometer?</h1>";
		echo "<p>De Asometer is als een project van de Hogeschool Rotterdam begonnen. In het team zitten Christian Vermeulen (<a href=\'http://twitter.com/christian__v\' target=\'_blank\'>@Christian__V</a>), Renee van Yperen (<a href=\'http://twitter.com/reneevyperen\' target=\'_blank\'>@reneevyperen</a>), Aza Lemmer en Jasper Lammens.</p>"
	?>')">Over Asometer</a> | <a href="mailto: info@asometer.nl">Contact</a> | <a href="http://twitter.com/asometer" target="_blank">Twitter</a> | &copy; 2011 - asometer.nl</p>
</div>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7869825-16']);
 	<?php if($_SESSION['token'] != ''){?>
 	_gaq.push(['_trackPageview', '/scored']);
	<?php } else {?>
	_gaq.push(['_trackPageview']);
	<?php }?>

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Piwik --> 
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://piwik.christianvermeulen.net/" : "http://piwik.christianvermeulen.net/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 5);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://piwik.christianvermeulen.net/piwik.php?idsite=5" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->
</body>
</html>