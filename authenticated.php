<?php 
	session_start();
	
	if($_SESSION['token'] == '')
		header("location: /tokenrequest.php");
		
	require_once("src/FoursquareAPI.class.php");
 	require_once("array.php");
 
	// Set your client key and secret
	$client_key = "40V3IN22EVZRSL0NNGGMVMPBU4HMNUNKX5JB0BLLPP1BFLJM";
	$client_secret = "GC3Q54OPKXUYVLARB21LXJ11X5ZNXQQDLPILEDWL3R1SOPBN";
	// Set your auth token, loaded using the workflow described in tokenrequest.php
	$auth_token = $_SESSION['token'];
	// Load the Foursquare API library
	$foursquare = new FoursquareAPI($client_key,$client_secret);
	$foursquare->SetAccessToken($auth_token);
?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title>Wat is jouw ASO-Factor?</title>
	<style type="text/css">
	body {
		font-family:Georgia;
		font-size: 16px;
		color:#333333;
		margin:0;
		padding:0;
	}
	
	h1 {
		width:100%;
		text-align:center;
		padding-top:20px;
		padding-bottom:20px;
		background-color:#2C76C9;
		color:#ffffff;
		margin:0;
	}
	ul,p {
		width:600px;
		margin:0;
		padding:0;
		margin:20px auto;
		list-style-type:none;
	}
	ul li
	{
		margin:0;
		padding:0;
		margin-bottom:20px;
		border-bottom: 2px solid #333;
	}
	ul li img
	{
		float:left;
		margin-right:10px;
	}
</style>
</head>
<body>
<?php	
	// Request doen!
	$params = array("limit"=>100);
	$response = $foursquare->GetPrivate("users/self");
	$users = json_decode($response);
	foreach($users->response as $response){
	?>
		<h1>Welkom <?php echo $response->firstName; ?></h1>
		<p>Jouw twitter is: <a href="http://twitter.com/<?php echo $response->contact->twitter;?>">@<?php echo $response->contact->twitter;?></a></p>
	<?php
	}
	?>
		<?php
		
			// Request doen!
			$params = array("limit"=>100);
			$response = $foursquare->GetPrivate("users/self/checkins",$params);
			$checkins = json_decode($response);

			
			// Wat settings
			$points = array();
			$score = 0;
			
			
			// Elke checkin bekijken
			foreach ($checkins->response->checkins->items as $checkin)
			{
				// Type locatie checken
				$catnames = "";
				$catnames = array();
				if(sizeof($checkin->venue->categories) != 0)
				{
					$found = false;
					
					foreach($checkin->venue->categories as $categorie)
					{	
						// Categorie naam
						if(in_array($categorie->name,$asocial))
						{
							$found = true;
							$c = array("name"=>$categorie->name,"icon"=>$categorie->icon);
							array_push($catnames,$c);
						}
						
						// Misschien is de parent al voldoende?
						foreach($categorie->parents as $parent)
						{
							if(in_array($parent,$asocial))
							{
								$found = true;
								$c = array("name"=>$parent);
								array_push($catnames,$c);
							}
						}
						
						
						if(sizeof($categorie) == 0)
							$found = false;
							
						
					}
				}
				
				if($found)
				{
					$point = array(
						"name" 		 => $checkin->venue->name,
						"time" 		 => $checkin->createdAt,
						"categories" => $catnames
					);
					
					if(sizeof($catnames) != 0)
					{
						$score++;
						array_push($points,$point);
					}
				}
			}
			
			echo "<p>Je ASO-Factor = ".$score."</p>";
		?>
	<ul>
		<?php
		
		function mdarray_to_string($array, $depth=0){
		$string .= "array( \n";
		$depth++;
		foreach($array as $key => $val){
			$string .=	get_indent_space($depth).quote_type_wrap($key). ' => ';
			if(is_array($val)){			
				$string .= mdarray_to_string($val,$depth).",\n";
			}	
			else
				$string .= quote_type_wrap($val).",\n";
		}
		$depth--;
		$string .= get_indent_space($depth).")";
		return $string;
	}
	function get_indent_space($depth){
		$output = '';
		for($i=0;$i<=$depth;$i++)
			$output.='  ';
		return $output;
	}
	function quote_type_wrap($var){
		switch(gettype($var)){
			case 'string':
				return '"'.$var.'"';
			case 'NULL':
				return "null";
			//TODO: handle other variable types.. ( objects? )
			default :
				return $var;
		}
	}
		
		foreach($points as $point)
		{
			?>
			<li>
				<img src="<?php echo $point['categories'][0]['icon'];?>"/>
				<strong><?php echo $point['name'];?></strong><br/>
				<?php
					foreach($point['categories'] as $cat)
					{
						echo "/ ".$cat['name']." ";
					}
					echo "/";
				?>
			</li>
			<?php
			//echo "<pre>".mdarray_to_string($point)."</pre>";
		}
		?>
	</ul>
	<p style="position:absolute;left:10px;top:90px;">
		<?php
		echo "<strong>Filter:</strong><br/>";
		foreach($asocial as $cat)
		{
			echo $cat."<br/>";
		}
		?>
	</p>
</body>
</html>
