<?php 
	session_start();	
	require_once("src/FoursquareAPI.class.php");
	require_once("array.php");
	
	// This file is intended to be used as your redirect_uri for the client on Foursquare
	
	// Set your client key and secret
	if($_SERVER['HTTP_HOST'] == 'beta.asometer.nl')
	{
		$client_key = "PY4JLKVKZEF5QGYPT14WJSZIDR3NYAFGLB2X4F4SEGUKLLOC";
		$client_secret = "VWW3TVH0FQRV5GBFTXWF34DKD5TVHZ4ZLNM223II5N4ZCHU2";
		$redirect_uri = "http://beta.asometer.nl/";
	}
	else if($_SERVER['HTTP_HOST'] == 'asometer.nl')
	{
		$client_key = "ZL3F0TAVKUPOLBBSHUUDMMR5DVR1CMM5Y14TAF52ZZ3FQYCF";
		$client_secret = "MLITWVPZM1J0Z51CDJPAJAGEOANIFB5NL0WFX1PXNQDZ414R";
		$redirect_uri = "http://asometer.nl/";
	}
	// Load the Foursquare API library
	$foursquare = new FoursquareAPI($client_key,$client_secret);
	
	// If the link has been clicked, and we have a supplied code, use it to request a token
	if(array_key_exists("code",$_GET)){
		$token = $foursquare->GetToken($_GET['code'],$redirect_uri);
		$_SESSION['token'] = $token;
		$auth_token = $_SESSION['token'];
		$foursquare->SetAccessToken($auth_token);
		header("location: /");
	}
	
	function mdarray_to_string($array, $depth=0){
		$string .= "array( \n";
		$depth++;
		foreach($array as $key => $val){
			$string .=	get_indent_space($depth).quote_type_wrap($key). ' => ';
			if(is_array($val)){			
				$string .= mdarray_to_string($val,$depth).",\n";
			}	
			else
				$string .= quote_type_wrap($val).",\n";
		}
		$depth--;
		$string .= get_indent_space($depth).")";
		return $string;
	}
	function get_indent_space($depth){
		$output = '';
		for($i=0;$i<=$depth;$i++)
			$output.='  ';
		return $output;
	}
	function quote_type_wrap($var){
		switch(gettype($var)){
			case 'string':
				return '"'.$var.'"';
			case 'NULL':
				return "null";
			//TODO: handle other variable types.. ( objects? )
			default :
				return $var;
		}
	}
?>
<!doctype html>
<html>
<head>
	<title>Asometer | Hoe asociaal ben jij?</title>
	<meta name="description" content="Hoe asociaal wordt jij door social media? Wij berkenen het voor je!">
	<meta name="author" content="Christianvermeulen.net">
	<link href="/css/style.css" rel="stylesheet"/>
	<link href="/css/apprise.min.css" rel="stylesheet"/>
	<link rel="shortcut icon" href="/img/favicon.gif">
	<script src="/js/jquery-1.3.2.min.js"></script>
	<script src="/js/apprise-1.5.min.js"></script>
</head>
<body>
<div class="asometer">
	<img alt="Asometer" src="/img/logo.png"/>
	<img alt="Hoe asociaal ben jij?" src="/img/sublogo.png"/>
	<?php 
	if($_SESSION['token'] != ''){
		$token = $foursquare->GetToken($_SESSION['token'],$redirect_uri);
		$auth_token = $_SESSION['token'];
		$foursquare->SetAccessToken($auth_token);
		?>
		<?php
		// Request doen!
		$params = array("limit"=>100);
		$response = $foursquare->GetPrivate("users/self");
		$users = json_decode($response);
		foreach($users->response as $response){
			$profile = $response;
		?>
			<!--
			<h1>Welkom <?php echo $response->firstName; ?></h1>
			<p>Jouw twitter is: <a href="http://twitter.com/<?php echo $response->contact->twitter;?>">@<?php echo $response->contact->twitter;?></a></p>
			 -->
		<?php 
		} 
		?>
			<?php
			
				// Request doen!
				$params = array("limit"=>100);
				$response = $foursquare->GetPrivate("users/self/checkins",$params);
				$checkins = json_decode($response);
	
				
				// Wat settings
				$points = array();
				$score = 0;
				$numcheckins = sizeof($checkins->response->checkins->items);
				$maincats = array(
					"Arts & Entertainment" 	=> 0,
					"Colleges & Universities" 	=> 0,
					"Food" 					=> 0,
					"Great Outdoors" 		=> 0,
					"Home, Work, Other" 	=> 0,
					"Nightlife Spots" 		=> 0,
					"Shops" 					=> 0,
					"Travel Spots" 			=> 0
				);
				
				// Elke checkin bekijken
				foreach ($checkins->response->checkins->items as $checkin)
				{
					// Type locatie checken
					$catnames = "";
					$catnames = array();
					
					
					
					if(sizeof($checkin->venue->categories) != 0)
					{
						$found = false;
						
						foreach($checkin->venue->categories as $categorie)
						{
							
							// Categorie naam
							if(in_array($categorie->name,$asocial))
							{
								$found = true;
								$c = array("name"=>$categorie->name,"icon"=>$categorie->icon);
								array_push($catnames,$c);
								
								//$maincats[$categorie->name] += 1;
							}
							
							// Misschien is de parent al voldoende?
							foreach($categorie->parents as $parent)
							{
								if(in_array($parent,$asocial))
								{
									$found = true;
									$c = array("name"=>$parent);
									array_push($catnames,$c);
								}
								
								if($found)
								{
									if(in_array($parent,$maincats))
										$maincats[$parent] += 1;
								}
							}
							
							
							if(sizeof($categorie) == 0)
								$found = false;
								
							
						}
					}
					
					if($found)
					{
						$point = array(
							"name" 		 => $checkin->venue->name,
							"time" 		 => $checkin->createdAt,
							"categories" => $catnames
						);
						
						if(sizeof($catnames) != 0)
						{
							$score++;
							array_push($points,$point);
						}
					}
				}
				
				$factor = ($score / $numcheckins) * 100;
				$factor = round($factor,2);
				
				
				
				//echo "<p>".$profile->firstName.", Social Media maakt jou <span class='factor'>".$factor."%</span> asocialer!</p>";
				
				array_splice($maincats,8);
				
				echo "<ul>";
				foreach($maincats as $key => $value)
				{
					if($value != 0)
					{
						switch($key)
						{
							case("Arts & Entertainment"):
								echo "<li><strong>".$key." (".$value." checkins)</strong><br/>Je hebt geen entree betaalt om met je telefoon te spelen!</li>";
								break;
							case("Colleges & Universities"):
								echo "<li><strong>".$key." (".$value." checkins)</strong><br/>Zou je niet beter bij de les blijven?</li>";
								break;
							case("Food" ):
								echo "<li><strong>".$key." (".$value." checkins)</strong><br/>Je mag niet eten met je mond vol, dus ook niet met je telefoon bezig zijn!</li>";
								break;
							case("Great Outdoors"):
								echo "<li><strong>".$key." (".$value." checkins)</strong><br/>Je kunt beter om je heen kijken en oppassen met oversteken!</li>";
								break;
							case("Home, Work, Other"):
								echo "<li><strong>".$key." (".$value." checkins)</strong><br/>De baas betaalt je niet om met je telefoon te spelen!</li>";
								break;
							case("Nightlife Spots"):
								echo "<li><strong>".$key." (".$value." checkins)</strong><br/>Moet echt iedereen weten waar je bent? Als het goed is zijn je vrienden al bij je!</li>";
								break;
							case("Shops"):
								echo "<li><strong>".$key." (".$value." checkins)</strong><br/>Een winkel is om te winkelen, niet om te laten zien wat je allemaal NIET koopt!</li>";
								break;
							case("Travel Spots"):
								echo "<li><strong>".$key." (".$value." checkins)</strong><br/>Je gaat toch op vakantie om even weg te zijn van thuis?</li>";
								break;	
						}
					}
				}
				echo "</ul>";
				echo "<p><span class='factor'>Jij bent ".$factor."% Aso!</p>";
			?>
			<div class="social">
		
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="http://<?php echo $_SERVER['HTTP_HOST'];?>" data-text="Ik ben <?php echo $factor;?>% asocialer geworden door social media! Hoe asociaal ben jij geworden?" data-count="vertical" data-via="Asometer" data-related="Christian__V:Developer van Asometer">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
		
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=193687547346815&amp;href=http%3A%2F%2Fasometer.nl&amp;send=false&amp;layout=box_count&amp;width=50&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=60" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:60px;margin-right:20px;" allowTransparency="true"></iframe>
		
		<!-- Place this tag in your head or just before your close body tag -->
		<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
		  {lang: 'nl'}
		</script>
		
		<!-- Place this tag where you want the +1 button to render -->
		<g:plusone size="tall" href="http://asometer.nl"></g:plusone>
		
		</div>
		<?php
	}
	else
	{
	?>
		<p>Laat je Foursquare checkins door ons analyseren en kijk hoe asociaal jij wordt van social media!</p>
		<?php 
		echo "<a href='".$foursquare->AuthenticationLink($redirect_uri)."'><img src='http://playfoursquare.s3.amazonaws.com/press/logo/connect-blue@2x.png'/></a>";
		?>
		<div class="social">
		
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="http://<?php echo $_SERVER['HTTP_HOST'];?>" data-text="Hoe asociaal ben jij geworden door Social Media?" data-count="vertical" data-via="Asometer" data-related="Christian__V:Developer van Asometer">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
		
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=193687547346815&amp;href=http%3A%2F%2Fasometer.nl&amp;send=false&amp;layout=box_count&amp;width=50&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=60" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:60px;margin-right:20px;" allowTransparency="true"></iframe>
		
		<!-- Place this tag in your head or just before your close body tag -->
		<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
		  {lang: 'nl'}
		</script>
		
		<!-- Place this tag where you want the +1 button to render -->
		<g:plusone size="tall" href="http://asometer.nl"></g:plusone>
		
		</div>
	<?php } ?>
</div>
<div class="footer">
	<p><a href="#" onclick="apprise('<?php
		echo "<h1>Wat is Asometer?</h1>";
		echo "<p>Asometer kijkt naar je afgelopen 100 checkins op foursquare en waar deze zijn. Door te kijken hoeveel van je checkins \&quot;asociaal\&quot; zijn berekent hij een asofactor.</p>";
		echo "<h1>Waarom is er een Asometer?</h1>";
		echo "<p>De smartphone markt explodeert, mobiel dataverkeerd breekt alle records en iedereen is in de ban van social media. Maar de maatschappij weet nog niet goed wat ze ermee aan moet.";
		echo " Wat vind men nou wel asociaal en wat niet? Om de maatschappij een handje te helpen om hier normen en waarden voor op te stellen hebben wij de asometer in het leven geroepen.";
		echo " Hetgeen de asometer als asociaal ziet hoeft dus niet <strong>per definitie</strong> asociaal te zijn. Het moet je alleen aan het denken zetten om zelf tot een conclusie te komen!</p>";
		echo "<h1>Wie zit er achter de Asometer?</h1>";
		echo "<p>De Asometer is als een project van de Hogeschool Rotterdam begonnen. In het team zitten Christian Vermeulen (<a href=\'http://twitter.com/christian__v\' target=\'_blank\'>@Christian__V</a>), Renee van Yperen (<a href=\'http://twitter.com/reneevyperen\' target=\'_blank\'>@reneevyperen</a>), Aza Lemmer en Jasper Lammens.</p>"
	?>')">Over Asometer</a> | <a href="mailto: info@asometer.nl">Contact</a> | <a href="http://twitter.com/asometer" target="_blank">Twitter</a> | &copy; 2011 - asometer.nl</p>
</div>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7869825-16']);
 	<?php if($_SESSION['token'] != ''){?>
 	_gaq.push(['_trackPageview', '/scored']);
	<?php } else {?>
	_gaq.push(['_trackPageview']);
	<?php }?>

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- Piwik --> 
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://piwik.christianvermeulen.net/" : "http://piwik.christianvermeulen.net/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 5);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://piwik.christianvermeulen.net/piwik.php?idsite=5" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->
</body>
</html>