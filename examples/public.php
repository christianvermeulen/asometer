<?php 
	require_once("../src/FoursquareAPI.class.php");
?>
<!doctype html>
<html>
<head>
	<title>PHP-Foursquare :: Unauthenticated Request Example</title>
</head>
<body>
<?php 
	// Set your client key and secret
	$client_key = "40V3IN22EVZRSL0NNGGMVMPBU4HMNUNKX5JB0BLLPP1BFLJM";
	$client_secret = "GC3Q54OPKXUYVLARB21LXJ11X5ZNXQQDLPILEDWL3R1SOPBN";
	// Load the Foursquare API library
	$foursquare = new FoursquareAPI($client_key,$client_secret);
	
	// Generate a latitude/longitude pair using Google Maps API
	list($lat,$lng) = $foursquare->GeoLocate($location);
	
	// Prepare parameters
	$params = array("ll"=>"$lat,$lng");
	
	// Perform a request to a public resource
	$response = $foursquare->GetPublic("venues/categories");
	$venues = json_decode($response);
	
	function mdarray_to_string($array, $depth=0){
		$string .= "array( \n";
		$depth++;
		foreach($array as $key => $val){
			//$string .=	get_indent_space($depth).quote_type_wrap($key). ' => ';
			if(is_array($val)){			
				$string .= mdarray_to_string($val,$depth).",\n";
			}	
			else
				$string .= quote_type_wrap($val).",\n";
		}
		$depth--;
		$string .= get_indent_space($depth).")";
		return $string;
	}
	function get_indent_space($depth){
		$output = '';
		for($i=0;$i<=$depth;$i++)
			$output.='  ';
		return $output;
	}
	function quote_type_wrap($var){
		switch(gettype($var)){
			case 'string':
				return '"'.$var.'"';
			case 'NULL':
				return "null";
			//TODO: handle other variable types.. ( objects? )
			default :
				return $var;
		}
	}
	
	$parents = array();
	
	foreach($venues->response->categories as $cat):
		
		$parents[] = $cat->name;

		foreach($cat->categories as $child):
			
			$parents[] = $child->name;

			foreach($child->categories as $grandchild):
				
				$parents[] = $grandchild->name;
				
			endforeach;
		
		endforeach;
		
	endforeach;
	
	echo "<pre>$asocial = ".mdarray_to_string($parents)."</pre>";
	?>
);
</body>
</html>
